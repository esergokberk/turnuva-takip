Proje ilk olarak Git üzerinden git clone https://gitlab.com/esergokberk/turnuva-takip.git ile belirlediğimiz dizine kopyalanır.

Maven -> Maven Settings -> Maven home path ile Bundled (Maven 3) seçimi yapılır.

Daha sonra veritabanı ayarlarımız için JPA Structure -> DB Connection -> New -> Detect Connection adımlarını izleyerek daha önce application.properties ayarlarında verdiğimiz veritabanı bilgilerine göre connection sağlarız.

Otomatik connection sağlamaması durumunda veritabanı bilgilerimiz şu şekildedir;

Url : jdbc:postgresql://localhost:5432/turnuvatakip
Username : postgres
Password : postgres
TurnuvaTakipApplication çalıştırılarak Spring Boot projesi localhost:8090 portu üzerinden ayağa kaldırılır.

PgAdmin4 üzerinden istersek tablolarımızı kontrol edebiliriz. Veritabanı için kullandığım kullanıcı adı ve şifre ile PgAdmin4 e ulaşmaktayım.

Swagger API adresi

http://localhost:8090/swagger-ui/#/

Spring Security Bilgileri

username: user
password: user

