package com.turnuva.turnuvatakip.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class OyuncuDto implements Serializable {
    private Long id;
    private Long takimId;
    private Long oyuncuId;
    private int numara;
}
