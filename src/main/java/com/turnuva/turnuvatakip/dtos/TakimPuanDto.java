package com.turnuva.turnuvatakip.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class TakimPuanDto implements Serializable {
    private Long id;
    private Long takimId;
    private int puan;
}
