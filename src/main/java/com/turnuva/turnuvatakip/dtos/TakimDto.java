package com.turnuva.turnuvatakip.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class TakimDto implements Serializable {
    private Long id;
    private Long turnuvaId;
    private String takimAdi;
    private Long takimSorumlusuId;
}
