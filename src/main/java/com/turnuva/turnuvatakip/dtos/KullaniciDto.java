package com.turnuva.turnuvatakip.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class KullaniciDto implements Serializable {
    private Long id;
    private String kullaniciAdi;
    private String adSoyad;
    private Long rolId;
    private int yas;
}
