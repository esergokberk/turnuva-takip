package com.turnuva.turnuvatakip.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class TurnuvaDto implements Serializable {
    private Long id;
    private int yil;
    private Long turnuvaTipiId;
}
