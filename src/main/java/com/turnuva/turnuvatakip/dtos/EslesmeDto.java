package com.turnuva.turnuvatakip.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class EslesmeDto implements Serializable {
    private Long id;
    private Long takim1Id;
    private Long takim2Id;
    private String skor;
}
