package com.turnuva.turnuvatakip.dtos;

import lombok.Data;

@Data
public class ApiErrorDto {
    private int status;
    private String message;
}
