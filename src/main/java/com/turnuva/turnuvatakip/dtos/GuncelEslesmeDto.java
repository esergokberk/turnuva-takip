package com.turnuva.turnuvatakip.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class GuncelEslesmeDto implements Serializable {
    private Long id;
    private String skor;
}
