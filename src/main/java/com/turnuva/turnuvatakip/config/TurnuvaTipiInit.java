package com.turnuva.turnuvatakip.config;

import com.turnuva.turnuvatakip.enums.TurnuvaTipiEnum;
import com.turnuva.turnuvatakip.models.TurnuvaTipi;
import com.turnuva.turnuvatakip.repository.TurnuvaTipiRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TurnuvaTipiInit {
    @Bean
    CommandLineRunner initDatabaseTurnuvaTipi(TurnuvaTipiRepository repository) {
        return args -> {
            for (TurnuvaTipiEnum turnuvaTipiEnum : TurnuvaTipiEnum.values()) {
                repository.findByTurnuvaTipi(turnuvaTipiEnum).orElseGet(() -> {
                    TurnuvaTipi turnuvaTipi = new TurnuvaTipi();
                    turnuvaTipi.setTurnuvaTipi(turnuvaTipiEnum);
                    return repository.save(turnuvaTipi);
                });
            }
        };
    }
}
