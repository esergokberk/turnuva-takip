package com.turnuva.turnuvatakip.config;

import com.turnuva.turnuvatakip.enums.RolEnum;
import com.turnuva.turnuvatakip.models.Rol;
import com.turnuva.turnuvatakip.repository.RolRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RolInit {
    @Bean
    CommandLineRunner initDatabaseRol(RolRepository repository) {
        return args -> {
            for (RolEnum rolEnum : RolEnum.values()) {
                repository.findByRol(rolEnum).orElseGet(() -> {
                    Rol rol = new Rol();
                    rol.setRol(rolEnum);
                    return repository.save(rol);
                });
            }
        };
    }
}
