package com.turnuva.turnuvatakip.services;

import com.turnuva.turnuvatakip.models.TakimPuan;
import com.turnuva.turnuvatakip.repository.TakimPuanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TakimPuanService {

    @Autowired
    private TakimPuanRepository takimPuanRepository;

    public List<TakimPuan> getSiralama(Long turnuvaId) {
        return takimPuanRepository.findByTakim_TurnuvaIdOrderByPuanDesc(turnuvaId);
    }

}
