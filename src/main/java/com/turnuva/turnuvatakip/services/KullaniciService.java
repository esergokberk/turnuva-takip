package com.turnuva.turnuvatakip.services;

import com.turnuva.turnuvatakip.dtos.KullaniciDto;
import com.turnuva.turnuvatakip.models.Kullanici;
import com.turnuva.turnuvatakip.models.Rol;
import com.turnuva.turnuvatakip.repository.KullaniciRepository;
import com.turnuva.turnuvatakip.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class KullaniciService {

    @Autowired
    private KullaniciRepository kullaniciRepository;

    @Autowired
    private RolRepository rolRepository;

    public Kullanici createKullanici(KullaniciDto kullaniciDto) {
        Kullanici kullanici = new Kullanici();
        kullanici.setKullaniciAdi(kullaniciDto.getKullaniciAdi());
        kullanici.setAdSoyad(kullaniciDto.getAdSoyad());
        kullanici.setYas(kullaniciDto.getYas());

        Optional<Rol> rol = rolRepository.findById(kullaniciDto.getRolId());
        kullanici.setRol(rol.get());

        return kullaniciRepository.save(kullanici);
    }

    public ResponseEntity<?> loginKullanici (String kullaniciAdi){
        Kullanici kullanici = kullaniciRepository.findByKullaniciAdi(kullaniciAdi);
        if(kullanici == null){
            return new ResponseEntity<>("Kullanıcı bulunamadı", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(kullanici, HttpStatus.OK);
    }

    public List<Kullanici> getKullanicilarByRolIds(List<Long> rolIds) {
        return kullaniciRepository.findByRol_IdIn(rolIds);
    }

}
