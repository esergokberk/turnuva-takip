package com.turnuva.turnuvatakip.services;

import com.turnuva.turnuvatakip.models.Rol;
import com.turnuva.turnuvatakip.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolService {

    @Autowired
    private RolRepository rolRepository;

    public List<Rol> getAllRol(){
        return rolRepository.findAll();
    }
}
