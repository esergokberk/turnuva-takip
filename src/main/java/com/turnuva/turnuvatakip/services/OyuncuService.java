package com.turnuva.turnuvatakip.services;

import com.turnuva.turnuvatakip.dtos.ApiErrorDto;
import com.turnuva.turnuvatakip.dtos.OyuncuDto;
import com.turnuva.turnuvatakip.models.Kullanici;
import com.turnuva.turnuvatakip.models.Oyuncu;
import com.turnuva.turnuvatakip.models.Takim;
import com.turnuva.turnuvatakip.repository.KullaniciRepository;
import com.turnuva.turnuvatakip.repository.OyuncuRepository;
import com.turnuva.turnuvatakip.repository.TakimRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class OyuncuService {

    @Autowired
    private OyuncuRepository oyuncuRepository;

    @Autowired
    private TakimRepository takimRepository;

    @Autowired
    private KullaniciRepository kullaniciRepository;


    public ResponseEntity<?> createOyuncu(OyuncuDto oyuncuDto) {
        try{
            Takim takim = takimRepository.findById(oyuncuDto.getTakimId()).orElseThrow(() -> new RuntimeException("Takım bulunamadı."));
            Kullanici kullanici = kullaniciRepository.findById(oyuncuDto.getOyuncuId()).orElseThrow(() -> new RuntimeException("Oyuncu bulunamadı."));

            if (oyuncuRepository.findByTakimAndOyuncu(takim, kullanici) != null) {
                throw new RuntimeException("Bu takımda bu oyuncu zaten var.");
            }

            if (oyuncuRepository.countByTakimAndOyuncu_YasLessThan(takim, 30) >= 3) {
                throw new RuntimeException("Bu takımda 30 yaşından küçük 3 oyuncu var.");
            }

            List<Takim> turnuvadakiTakimlar = takimRepository.findByTurnuvaId(takim.getTurnuva().getId());
            for (Takim t : turnuvadakiTakimlar) {
                if (t != takim && t.getTakimSorumlusu().equals(kullanici)) {
                    throw new RuntimeException("Oyuncu bu turnuvada başka bir takımın sorumlusu.");
                }
                Oyuncu turnuvadakiOyuncu = oyuncuRepository.findByTakimAndOyuncu(t, kullanici);
                if (turnuvadakiOyuncu != null) {
                    throw new RuntimeException("Oyuncu bu turnuvada başka bir takımda zaten var.");
                }
            }

            Oyuncu oyuncu = new Oyuncu();
            oyuncu.setTakim(takim);
            oyuncu.setOyuncu(kullanici);
            oyuncu.setNumara(oyuncuDto.getNumara());

            oyuncuRepository.save(oyuncu);
            return new ResponseEntity<>(oyuncu, HttpStatus.OK);
        } catch (RuntimeException e) {
            ApiErrorDto apiError = new ApiErrorDto();
            apiError.setStatus(HttpStatus.BAD_REQUEST.value());
            apiError.setMessage(e.getMessage());
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }

    }

    public void deleteOyuncu(Long id) {
        oyuncuRepository.deleteById(id);
    }

}

