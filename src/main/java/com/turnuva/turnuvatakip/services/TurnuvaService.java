package com.turnuva.turnuvatakip.services;


import com.turnuva.turnuvatakip.dtos.ApiErrorDto;
import com.turnuva.turnuvatakip.dtos.TurnuvaDto;
import com.turnuva.turnuvatakip.models.Turnuva;
import com.turnuva.turnuvatakip.models.TurnuvaTipi;
import com.turnuva.turnuvatakip.repository.TurnuvaRepository;
import com.turnuva.turnuvatakip.repository.TurnuvaTipiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TurnuvaService {

    @Autowired
    private TurnuvaRepository turnuvaRepository;

    @Autowired
    private TurnuvaTipiRepository turnuvaTipiRepository;


    public ResponseEntity<?> createTurnuva(TurnuvaDto turnuvaDto){
        try {
            TurnuvaTipi turnuvaTipi = turnuvaTipiRepository.findById(turnuvaDto.getTurnuvaTipiId()).get();
            Turnuva turnuva = new Turnuva();
            turnuva.setTurnuvaTipi(turnuvaTipi);
            turnuva.setYil(turnuvaDto.getYil());
            if(turnuvaRepository.findByYilAndTurnuvaTipi(turnuva.getYil(), turnuva.getTurnuvaTipi()) != null) {
                throw new RuntimeException("Bu yıl ve turnuva tipi ile kayıtlı bir turnuva zaten var.");
            }
            turnuvaRepository.save(turnuva);
            return new ResponseEntity<>(turnuva, HttpStatus.OK);
        } catch (RuntimeException e) {
            ApiErrorDto apiError = new ApiErrorDto();
            apiError.setStatus(HttpStatus.BAD_REQUEST.value());
            apiError.setMessage(e.getMessage());
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
    }

    public List<Turnuva> getTurnuvaList(int yil){
        return turnuvaRepository.findByYil(yil);
    }

    public List<TurnuvaTipi> getTurnuvaTipiList(){
        return turnuvaTipiRepository.findAll();
    }
}
