package com.turnuva.turnuvatakip.services;

import com.turnuva.turnuvatakip.dtos.EslesmeDto;
import com.turnuva.turnuvatakip.dtos.GuncelEslesmeDto;
import com.turnuva.turnuvatakip.models.Eslesme;
import com.turnuva.turnuvatakip.models.Takim;
import com.turnuva.turnuvatakip.models.TakimPuan;
import com.turnuva.turnuvatakip.repository.EslesmeRepository;
import com.turnuva.turnuvatakip.repository.TakimPuanRepository;
import com.turnuva.turnuvatakip.repository.TakimRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EslesmeService {

    @Autowired
    private EslesmeRepository eslesmeRepository;

    @Autowired
    private TakimPuanRepository takimPuanRepository;

    @Autowired
    private TakimRepository takimRepository;


    public Eslesme createEslesme(EslesmeDto eslesmeDto) {
        Eslesme eslesme = new Eslesme();
        Takim takim1 = takimRepository.findById(eslesmeDto.getTakim1Id()).get();
        Takim takim2 = takimRepository.findById(eslesmeDto.getTakim2Id()).get();
        eslesme.setTakim1(takim1);
        eslesme.setTakim2(takim2);
        eslesme.setSkor(eslesmeDto.getSkor());

        return eslesmeRepository.save(eslesme);
    }

    public Eslesme updateEslesme(GuncelEslesmeDto guncelEslesmeDto) {
        Eslesme eslesme = eslesmeRepository.findById(guncelEslesmeDto.getId())
                .orElseThrow(() -> new RuntimeException("Eslesme bulunamadi"));

        eslesme.setSkor(guncelEslesmeDto.getSkor());

        return eslesmeRepository.save(eslesme);
    }


    public void updateTakimPuan(Eslesme eslesme) {

        String[] skorlar = eslesme.getSkor().split("-");
        int takim1Skor = Integer.parseInt(skorlar[0]);
        int takim2Skor = Integer.parseInt(skorlar[1]);

        if (takim1Skor > takim2Skor) {
            TakimPuan takim1Puan = takimPuanRepository.findByTakimId(eslesme.getTakim1().getId());
            takim1Puan.setPuan(takim1Puan.getPuan() + 3);
            takimPuanRepository.save(takim1Puan);
        } else if (takim1Skor < takim2Skor) {
            TakimPuan takim2Puan = takimPuanRepository.findByTakimId(eslesme.getTakim2().getId());
            takim2Puan.setPuan(takim2Puan.getPuan() + 3);
            takimPuanRepository.save(takim2Puan);
        } else {
            TakimPuan takim1Puan = takimPuanRepository.findByTakimId(eslesme.getTakim1().getId());
            takim1Puan.setPuan(takim1Puan.getPuan() + 1);
            takimPuanRepository.save(takim1Puan);

            TakimPuan takim2Puan = takimPuanRepository.findByTakimId(eslesme.getTakim2().getId());
            takim2Puan.setPuan(takim2Puan.getPuan() + 1);
            takimPuanRepository.save(takim2Puan);

        }

    }

    public List<Eslesme> getTurnuvaEslesmeList(Long turnuvaId) {
        return eslesmeRepository.findByTakim1_Turnuva_IdOrTakim2_Turnuva_Id(turnuvaId, turnuvaId);
    }
}
