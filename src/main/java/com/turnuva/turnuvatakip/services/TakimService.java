package com.turnuva.turnuvatakip.services;

import com.turnuva.turnuvatakip.dtos.ApiErrorDto;
import com.turnuva.turnuvatakip.dtos.TakimDto;
import com.turnuva.turnuvatakip.models.Oyuncu;
import com.turnuva.turnuvatakip.models.Takim;
import com.turnuva.turnuvatakip.models.TakimPuan;
import com.turnuva.turnuvatakip.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TakimService {

    @Autowired
    private TakimRepository takimRepository;

    @Autowired
    private TurnuvaRepository turnuvaRepository;

    @Autowired
    private KullaniciRepository kullaniciRepository;

    @Autowired
    private OyuncuRepository oyuncuRepository;

    @Autowired
    private TakimPuanRepository takimPuanRepository;

    public ResponseEntity<?> createTakim(TakimDto takimDto){
        try{
        Takim takim = new Takim();
        takim.setTakimAdi(takimDto.getTakimAdi());
        takim.setTurnuva(turnuvaRepository.findById(takimDto.getTurnuvaId()).get());
        takim.setTakimSorumlusu(kullaniciRepository.findById(takimDto.getTakimSorumlusuId()).get());
        if(takimRepository.findByTurnuvaAndTakimAdi(takim.getTurnuva(), takim.getTakimAdi()) != null) {
            throw new RuntimeException("Bu turnuvada bu takım adı ile kayıtlı bir takım zaten var.");
        }
        if(takimRepository.findByTurnuvaAndTakimSorumlusu(takim.getTurnuva(), takim.getTakimSorumlusu()) != null) {
            throw new RuntimeException("Bu turnuvada bu takım sorumlusu ile kayıtlı bir takım zaten var.");
        }
        Takim savedTakim = takimRepository.save(takim);

        TakimPuan takimPuan = new TakimPuan();
        takimPuan.setTakim(savedTakim);
        takimPuan.setPuan(0);

        if(takimPuanRepository.findByTakimId(takimPuan.getTakim().getId()) != null) {
            throw new RuntimeException("Bu takım id'sine sahip bir puan kaydı zaten var.");
        }

        takimPuanRepository.save(takimPuan);
            return new ResponseEntity<>(savedTakim, HttpStatus.OK);
        } catch (RuntimeException e) {
            ApiErrorDto apiError = new ApiErrorDto();
            apiError.setStatus(HttpStatus.BAD_REQUEST.value());
            apiError.setMessage(e.getMessage());
            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
        }
    }

    public List<Oyuncu> getTakimOyuncuList(Long takimId) {
        Takim takim = takimRepository.findById(takimId)
                .orElseThrow(() -> new RuntimeException("Takım bulunamadı"));
        return oyuncuRepository.findAllByTakim(takim);
    }

    public List<Takim> getTurnuvaTakimList(Long turnuvaId) {
        return takimRepository.findByTurnuvaId(turnuvaId);
    }

}
