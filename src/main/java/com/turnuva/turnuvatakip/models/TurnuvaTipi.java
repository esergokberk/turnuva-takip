package com.turnuva.turnuvatakip.models;

import com.turnuva.turnuvatakip.enums.TurnuvaTipiEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "turnuva_tipi")
@Getter
@Setter
public class TurnuvaTipi implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "turnuva_tipi" , nullable = false, length = 20)
    private TurnuvaTipiEnum turnuvaTipi;


}
