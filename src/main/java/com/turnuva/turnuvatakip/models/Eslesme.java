package com.turnuva.turnuvatakip.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "eslesme")
@Getter
@Setter
public class Eslesme implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "takim_id1", nullable = false)
    private Takim takim1;

    @ManyToOne
    @JoinColumn(name = "takim_id2", nullable = false)
    private Takim takim2;

    @Column(name = "skor")
    private String skor;

}
