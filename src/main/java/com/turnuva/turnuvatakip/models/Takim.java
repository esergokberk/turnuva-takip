package com.turnuva.turnuvatakip.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "takim",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"turnuva_id", "takim_adi"}),
                @UniqueConstraint(columnNames = {"turnuva_id", "takim_sorumlusu"})
        }
)
@Getter
@Setter
public class Takim implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "turnuva_id", nullable = false)
    private Turnuva turnuva;

    @Column(name = "takim_adi", nullable = false)
    private String takimAdi;

    @ManyToOne
    @JoinColumn(name = "takim_sorumlusu", nullable = false)
    private Kullanici takimSorumlusu;

}
