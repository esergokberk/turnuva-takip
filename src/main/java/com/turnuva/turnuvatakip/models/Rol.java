package com.turnuva.turnuvatakip.models;

import com.turnuva.turnuvatakip.enums.RolEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "rol")
@Getter
@Setter
public class Rol implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "rol" , nullable = false, length = 20)
    private RolEnum rol;

}