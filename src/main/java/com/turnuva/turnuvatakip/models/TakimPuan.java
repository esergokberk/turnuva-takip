package com.turnuva.turnuvatakip.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "takim_puan")
@Getter
@Setter
public class TakimPuan implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "takim_id", referencedColumnName = "id")
    private Takim takim;

    @Column(name = "puan", nullable = false)
    private int puan = 0;
}
