package com.turnuva.turnuvatakip.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
@Getter
@Setter
public class Oyuncu implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "takim_id", referencedColumnName = "id")
    private Takim takim;

    @ManyToOne
    @JoinColumn(name = "oyuncu_id", referencedColumnName = "id")
    private Kullanici oyuncu;

    @Column(name = "numara")
    private int numara;
}
