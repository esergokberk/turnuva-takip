package com.turnuva.turnuvatakip.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "turnuva",
        uniqueConstraints = @UniqueConstraint(columnNames = {"yil", "turnuva_tipi_id"})
)
@Getter
@Setter
public class Turnuva implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "yil", nullable = false)
    private int yil;

    @ManyToOne
    @JoinColumn(name = "turnuva_tipi_id", nullable = false)
    private TurnuvaTipi turnuvaTipi;



}