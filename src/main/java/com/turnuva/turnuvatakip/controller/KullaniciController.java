package com.turnuva.turnuvatakip.controller;

import com.turnuva.turnuvatakip.dtos.KullaniciDto;
import com.turnuva.turnuvatakip.models.Kullanici;
import com.turnuva.turnuvatakip.services.KullaniciService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/kullanici", produces = "application/json")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class KullaniciController {

    @Autowired
    private KullaniciService kullaniciService;

    @ApiOperation(value = "Kullanıcı oluşturur.")
    @PostMapping(path = "/create")
    public Kullanici createKullanici(@ApiParam(value = "Oluşturulması istenen kullanıcı bilgileri",required = true) @RequestBody KullaniciDto kullaniciDto) {
        return kullaniciService.createKullanici(kullaniciDto);
    }

    @ApiOperation(value = "Kullanıcı kontrolü yapar.")
    @PostMapping(path = "/login")
    public ResponseEntity<?> loginKullanici(@ApiParam(value = "Kullanıcı adı",required = true) @RequestParam(value = "kullaniciAdi") String kullaniciAdi) {
        return kullaniciService.loginKullanici(kullaniciAdi);
    }

    @ApiOperation(value = "Kullanıcı Rolüne göre kullanıcıları getirir.")
    @GetMapping("/getKullanicilarByRolIds")
    public List<Kullanici> getKullanicilarByRolIds(@RequestParam("rolIds") String rolIds) {
        String[] rolIdArray = rolIds.split(",");
        List<Long> rolIdList = new ArrayList<>();
        for (String rolId : rolIdArray) {
            rolIdList.add(Long.parseLong(rolId));
        }
        return kullaniciService.getKullanicilarByRolIds(rolIdList);
    }

}
