package com.turnuva.turnuvatakip.controller;

import com.turnuva.turnuvatakip.dtos.OyuncuDto;
import com.turnuva.turnuvatakip.services.OyuncuService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/oyuncu", produces = "application/json")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class OyuncuController {

    @Autowired
    private OyuncuService oyuncuService;

    @ApiOperation(value = "Oyuncu oluşturur.")
    @PostMapping(path = "/create")
    public ResponseEntity<?> createOyuncu(@ApiParam(value = "Oyuncu bilgileri",required = true) @RequestBody OyuncuDto oyuncuDto) {
        return oyuncuService.createOyuncu(oyuncuDto);
    }

    @ApiOperation(value = "Oyuncu siler.")
    @DeleteMapping(path = "/delete")
    public void deleteOyuncu(@ApiParam(value = "Silinecek Oyuncu ID si",required = true) @RequestParam(value = "oyuncuId") Long oyuncuId) {
        oyuncuService.deleteOyuncu(oyuncuId);
    }
}
