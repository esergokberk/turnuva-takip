package com.turnuva.turnuvatakip.controller;

import com.turnuva.turnuvatakip.dtos.TurnuvaDto;
import com.turnuva.turnuvatakip.models.Turnuva;
import com.turnuva.turnuvatakip.models.TurnuvaTipi;
import com.turnuva.turnuvatakip.services.TurnuvaService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/turnuva", produces = "application/json")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TurnuvaController {

    @Autowired
    private TurnuvaService turnuvaService;

    @ApiOperation(value = "Turnuva oluşturur.")
    @PostMapping(path = "/create")
    public ResponseEntity<?> createTurnuva(@ApiParam(value = "Turnuva bilgileri",required = true) @RequestBody TurnuvaDto turnuvaDto) {
        return turnuvaService.createTurnuva(turnuvaDto);
    }

    @ApiOperation(value = "O yıla ait turnuva bilgilerini getirir.")
    @GetMapping(path = "/getTurnuvaList")
    public List<Turnuva> getTurnuvaList(@ApiParam(value = "Getirilmesi istenen yıl",required = true) @RequestParam(value = "yil") int yil) {
        return turnuvaService.getTurnuvaList(yil);
    }

    @ApiOperation(value = "Turnuva tiplerini getirir")
    @GetMapping(path = "/getTurnuvaTipList")
    public List<TurnuvaTipi> getTurnuvaTipiList(){
        return turnuvaService.getTurnuvaTipiList();
    }
}
