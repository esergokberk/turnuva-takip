package com.turnuva.turnuvatakip.controller;

import com.turnuva.turnuvatakip.dtos.TakimDto;
import com.turnuva.turnuvatakip.models.Oyuncu;
import com.turnuva.turnuvatakip.models.Takim;
import com.turnuva.turnuvatakip.services.TakimService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/takim", produces = "application/json")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TakimController {

    @Autowired
    private TakimService takimService;

    @ApiOperation(value = "Takım oluşturur.")
    @PostMapping(path = "/create")
    public ResponseEntity<?> createTakim(@ApiParam(value = "Takım bilgileri",required = true) @RequestBody TakimDto takimDto) {
        return takimService.createTakim(takimDto);
    }

    @ApiOperation(value = "Takım oyuncularını getirir.")
    @GetMapping(path = "/getTakimOyuncuList")
    public List<Oyuncu> getTakimOyuncuList(@ApiParam(value = "Getirilmesi istenen Takım ID si",required = true) @RequestParam(value = "takimId") Long takimId) {
        return takimService.getTakimOyuncuList(takimId);
    }

    @ApiOperation(value = "O turnuvaya ait takımları getirir.")
    @GetMapping(path = "/getTurnuvaTakimList")
    public List<Takim> getTurnuvaTakimList(@ApiParam(value = "Getirilmesi istenen Turnuva ID si",required = true) @RequestParam(value = "turnuvaId") Long turnuvaId) {
        return takimService.getTurnuvaTakimList(turnuvaId);
    }
}
