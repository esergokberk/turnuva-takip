package com.turnuva.turnuvatakip.controller;

import com.turnuva.turnuvatakip.models.Rol;
import com.turnuva.turnuvatakip.services.RolService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/kullanici-rol", produces = "application/json")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RolController {

    @Autowired
    private RolService rolService;

    @ApiOperation(value = "Kullanıcı Rol listesini getirir.")
    @GetMapping(path = "/list")
    public List<Rol> getAllRol(){
        return rolService.getAllRol();
    }
}
