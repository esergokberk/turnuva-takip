package com.turnuva.turnuvatakip.controller;

import com.turnuva.turnuvatakip.models.TakimPuan;
import com.turnuva.turnuvatakip.services.TakimPuanService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/takim-puan", produces = "application/json")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TakimPuanController {

    @Autowired
    private TakimPuanService takimPuanService;

    @ApiOperation(value = "Turnuva takımlarının sıralamalarını ve puanlarını getirir.")
    @GetMapping("/siralama")
    public List<TakimPuan> getSiralama(@ApiParam(value = "Sıralaması getirilecek olan Turnuva Id") @RequestParam(value = "turnuvaId") Long turnuvaId) {
        return takimPuanService.getSiralama(turnuvaId);
    }

}
