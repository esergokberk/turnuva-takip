package com.turnuva.turnuvatakip.controller;


import com.turnuva.turnuvatakip.dtos.EslesmeDto;
import com.turnuva.turnuvatakip.dtos.GuncelEslesmeDto;
import com.turnuva.turnuvatakip.models.Eslesme;
import com.turnuva.turnuvatakip.services.EslesmeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/eslesme", produces = "application/json")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EslesmeController {

    @Autowired
    private EslesmeService eslesmeService;

    @ApiOperation(value = "Eslesme oluşturulur.")
    @PostMapping(path = "/create")
    public Eslesme createEslesme(@ApiParam(value = "Eslesme bilgileri") @RequestBody EslesmeDto eslesmeDto) {
        return eslesmeService.createEslesme(eslesmeDto);
    }


    @ApiOperation(value = "Önce Eslesme sonra takımların puanı güncellenir.")
    @PutMapping(path = "/update")
    public Eslesme updateEslesmePuan(@ApiParam(value = "Guncellenecek eslesme bilgileri") @RequestBody GuncelEslesmeDto guncelEslesmeDto) {
        Eslesme updatedEslesme = eslesmeService.updateEslesme(guncelEslesmeDto);
        eslesmeService.updateTakimPuan(updatedEslesme);
        return updatedEslesme;
    }

    @ApiOperation(value = "Turnuvaya ait tüm eslesmeleri getirir.")
    @GetMapping(path = "/getTurnuvaEslesmeList")
    public List<Eslesme> getTurnuvaEslesmeList(@ApiParam(value = "Getirilmesi istenen Turnuva ID si",required = true) @RequestParam(value = "turnuvaId") Long turnuvaId) {
        return eslesmeService.getTurnuvaEslesmeList(turnuvaId);
    }

}
