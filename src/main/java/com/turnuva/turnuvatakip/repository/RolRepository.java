package com.turnuva.turnuvatakip.repository;


import com.turnuva.turnuvatakip.enums.RolEnum;
import com.turnuva.turnuvatakip.models.Rol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RolRepository extends JpaRepository<Rol, Long> {
    Optional<Rol> findByRol(RolEnum rol);

}
