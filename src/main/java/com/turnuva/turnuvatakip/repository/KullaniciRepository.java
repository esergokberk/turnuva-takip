package com.turnuva.turnuvatakip.repository;

import com.turnuva.turnuvatakip.models.Kullanici;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KullaniciRepository extends JpaRepository<Kullanici, Long> {

    Kullanici findByKullaniciAdi(String kullaniciAdi);

    List<Kullanici> findByRol_IdIn(List<Long> rolIds);

}