package com.turnuva.turnuvatakip.repository;

import com.turnuva.turnuvatakip.models.Turnuva;
import com.turnuva.turnuvatakip.models.TurnuvaTipi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TurnuvaRepository extends JpaRepository<Turnuva, Long> {

    Turnuva findByYilAndTurnuvaTipi(int yil, TurnuvaTipi turnuvaTipi);

    List<Turnuva> findByYil(int yil);

}