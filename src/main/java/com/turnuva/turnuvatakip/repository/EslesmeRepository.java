package com.turnuva.turnuvatakip.repository;

import com.turnuva.turnuvatakip.models.Eslesme;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EslesmeRepository extends JpaRepository<Eslesme, Long> {

    List<Eslesme> findByTakim1_Turnuva_IdOrTakim2_Turnuva_Id(Long turnuvaId1, Long turnuvaId2);}