package com.turnuva.turnuvatakip.repository;

import com.turnuva.turnuvatakip.models.TakimPuan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TakimPuanRepository extends JpaRepository<TakimPuan, Long> {

    TakimPuan findByTakimId(Long takimId);

    List<TakimPuan> findByTakim_TurnuvaIdOrderByPuanDesc(Long turnuvaId);
}