package com.turnuva.turnuvatakip.repository;

import com.turnuva.turnuvatakip.models.Kullanici;
import com.turnuva.turnuvatakip.models.Takim;
import com.turnuva.turnuvatakip.models.Turnuva;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TakimRepository extends JpaRepository<Takim, Long> {

    Takim findByTurnuvaAndTakimAdi(Turnuva turnuva, String takimAdi);

    Takim findByTurnuvaAndTakimSorumlusu(Turnuva turnuva, Kullanici takimSorumlusu);

    List<Takim> findByTurnuvaId(Long turnuvaId);

}