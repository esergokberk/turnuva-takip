package com.turnuva.turnuvatakip.repository;

import com.turnuva.turnuvatakip.enums.TurnuvaTipiEnum;
import com.turnuva.turnuvatakip.models.TurnuvaTipi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TurnuvaTipiRepository extends JpaRepository<TurnuvaTipi, Long> {

    Optional<TurnuvaTipi> findByTurnuvaTipi(TurnuvaTipiEnum turnuvaTipi);
}