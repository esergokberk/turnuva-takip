package com.turnuva.turnuvatakip.repository;

import com.turnuva.turnuvatakip.models.Kullanici;
import com.turnuva.turnuvatakip.models.Oyuncu;
import com.turnuva.turnuvatakip.models.Takim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OyuncuRepository extends JpaRepository<Oyuncu, Long> {

    Oyuncu findByTakimAndOyuncu(Takim takim, Kullanici oyuncu);

    int countByTakimAndOyuncu_YasLessThan(Takim takim, int yas);

    List<Oyuncu> findAllByTakim(Takim takim);
}