package com.turnuva.turnuvatakip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class TurnuvaTakipApplication {

	public static void main(String[] args) {
		SpringApplication.run(TurnuvaTakipApplication.class, args);
	}

}
